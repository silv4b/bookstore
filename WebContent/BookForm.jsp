<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Loja de livros | Cadastro</title>
</head>
<body>
	<div align="center">
		<h1>Gerenciamento de livros</h1>
	</div>

	<div align="center">
		<c:if test="${book != null}">
			<form action="update" method="post">
		</c:if>
		<c:if test="${book == null}">
			<form action="insert" method="post">
		</c:if>
		<table border="1" cellpadding="5">
			<caption>
				<h2>
					<c:if test="${book != null}">
            			Edit Book
            		</c:if>
					<c:if test="${book == null}">
            			Adicionar novo livro
            			<!-- Add New Book -->
					</c:if>
				</h2>
			</caption>
			<c:if test="${book != null}">
				<input type="hidden" name="id" value="<c:out value='${book.id}' />" />
			</c:if>
			<tr>
				<th>T�tulo:</th>
				<td><input type="text" name="title" size="45"
					value="<c:out value='${book.title}' />" /></td>
			</tr>
			<tr>
				<th>Autor:</th>
				<td><input type="text" name="author" size="45"
					value="<c:out value='${book.author}' />" /></td>
			</tr>
			<tr>
				<th>Pre�o:</th>
				<td><input type="text" name="price" size="5"
					value="<c:out value='${book.price}' />" /></td>
			</tr>
		</table>
		<br>
		<div align="center">
			<a colspan="2" align="center"><input type="submit" value="Salvar" /></a>
		</div>
		</form>
	</div>
	<div align="center">
		<h2>
			<a href="list"><button>Voltar</button></a>
		</h2>
	</div>
</body>
</html>
