<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Loja de livros | In�cio</title>
<style>
	table.sample {
		border-width: 1px;
		border-spacing: 1px;
		border-color: black;
		border-style: outset;
		border-collapse: separate;
		border-style: outset;
	}
	
	#imgpos {
		position: absolute;
		left: 50%;
		top: 50%;
		margin-left: -110px;
		margin-top: -40px;
	}
</style>
</head>
<body>
	<div id="img">
		<div align="center">
			<h1>Loja de livros</h1>
		</div>
		<div align="center">
			<div align="center">
				<h2>Listagem de livros</h2>
			</div>
			<table border="1" cellpadding="7" class="sample">
				<tr>
					<th>Chave</th>
					<th>T�tulo</th>
					<th>Autor</th>
					<th>Pre�o</th>
					<th>A��o</th>
				</tr>
				<c:forEach var="book" items="${listBook}">
					<tr>
						<td align="center"><c:out value="${book.id}" /></td>
						<td><c:out value="${book.title}" /></td>
						<td><c:out value="${book.author}" /></td>
						<td><c:out value="${book.price}" /></td>
						<td><a href="edit?id=<c:out value='${book.id}' />"><button>Editar</button></a>
							&nbsp;&nbsp; <a href="delete?id=<c:out value='${book.id}' />"><button>Deletar</button></a></td>
					</tr>
				</c:forEach>
			</table>
		</div>
		<div align="center">
			<h2>
				<a href="new"><button>Adicionar novo livro</button></a>
			</h2>
		</div>
	</div>
</body>
</html>
