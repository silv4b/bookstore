# Projeto POO II - Design Partners
### Implementar no mínimo 3 padrões de projetos abordados em aula.
### BookStore (Loja de livros [listagem CRUD] ).
Projeto Maven criado na IDE Eclipse (Photon Release Candidate 3 (4.8.0RC3) Build id: 20180607-0625)

  - Importar projeto como **Existing Maven Projects**.
  - Selecionar a opção **Update Project** (para baixas as dependências do pom.xml).
  - Configurar o **Target Runtimes** para **Apache Toncat vX.0** nas **Propriedades do projeto** (para poder acessar o link com porta local).
  - Criar o banco de dados (MySQL nesse exemplo) com o script que acompanha localizado na pasta **sql**.
  - Alterar na classe conexão, os dados referente à login e senha do seu localhost na classe **ControllerServlet**.
  - Para "rodar" o projeto, executar o menu **Run as**, na opção **Run on server** (apache toncat já configurado).
  - Acessar o link **http://localhost:8080/Bookstore/** onde 8080 seja a sua porta configurada.
    - **http://localhost:8080/Bookstore/list**
    - **http://localhost:8080/Bookstore/new**

**Corre pro abraço ♥**