package com.poo.bookstore.factory;

import com.poo.bookstore.model.Book;

public class FabricaLivro {
	
	public Book getLivro(int id) {
		return new Book (id);	
	}
	
	public Book getLivro(String nome, String autor, float preco) {
		return new Book (nome, autor, preco);	
	}
	
	public Book getLivro(int id, String nome, String autor, float preco) {
		return new Book (id, nome, autor, preco);	
	}
}