package com.poo.bookstore.bd;

public class DataConfigSingleton {
	
	private static DataConfigSingleton instancia;
	
	private String dbName = "Bookstore";
	private String urlp = "jdbc:mysql://localhost:3306/";
	private String url = urlp.concat(dbName);
	private String driver = "com.mysql.jdbc.Driver";
	private String userName = "root";
	private String password = "root";
	
	private DataConfigSingleton() {
		
	}
	
	public static synchronized DataConfigSingleton getInstancia () {
		if (instancia == null) {
			instancia = new DataConfigSingleton();
		}
		return instancia;
	}

	public String getUrl() {
		return url;
	}

	public String getUserName() {
		return userName;
	}

	public String getPassword() {
		return password;
	}
	
	public String getDriver() {
		return driver;
	}
}
